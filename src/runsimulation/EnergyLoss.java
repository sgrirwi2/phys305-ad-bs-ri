package runsimulation;

class EnergyLoss
{
    double rho;
    double Z;
    double A;
    
    public EnergyLoss() 
    {
        rho = A = Z = 0; //set to zero if no input
    }
    
    public EnergyLoss(double rho_in, double Z_in, double A_in)
    {
    rho = rho_in;   
    Z = Z_in;
    A = A_in;
    }
    
    public double getEnergyLoss(Particle p)
    {
         // shall return energy loss in MeV/m
        if (rho==0 && A==0 && Z==0){
            return 0;
        } else {
        double K = 0.307075; // MeV cm^2
        double m_e = 0.511; // MeV electron mass
        double Wmax = 2*m_e*Math.pow(p.beta(),2)*Math.pow(p.gamma(),2)/(1+(2*p.gamma()*m_e/p.m)+(Math.pow(m_e/p.m , 2))); // Max E transfer in single collision
        double I = 0.0000135 * Z;
        double square_brackets = 0.5*Math.log(2*m_e*Math.pow(p.beta(),2)*Math.pow(p.gamma(),2)*Wmax/Math.pow(I,2))-Math.pow(p.beta(),2); // square bracket part of dE/dx
        double ELoss = 100*K*Math.pow(p.Q,2)*rho*(Z/A)*Math.pow(1/p.beta(),2)*square_brackets;
        return ELoss;
        }
    }
    
}