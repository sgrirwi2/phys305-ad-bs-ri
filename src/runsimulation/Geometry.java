package runsimulation;

import java.util.Random;

class Geometry
{
    // Class to define the Geometry of an experiment
    // the "type" is a number defining the basic geometric object
    // the "rho_Z_A" array stores material information: 
    //    density, atomic number (nucleus charge), mass number (protons+neutrons)
    // the "shape" array stores the geometrical information
    //   shape = 6 values representing two corners of the internal diagonal (x0,y0,z0), (x1,y1,z1)
    // every basic object can be identified by a number
    // the first object is taken as to be the "world",
    //    i.e. we can use it to abort the simulation
    // maxShapes is the maximum allowed number of basic objects, extend if needed

    // the class also impelments thee additional helper functions:
    // * doEloss -- calculate EnergyLoss via associated class and apply to Particle
    // * doMultScatter -- calculate MultipleScattering via associated class and apply to Particle
    // * detectParticle -- simulate a detected particle position
    
    private Random randGen;
    private static final int maxShapes = 100;
    private int nshapes;
    private int [] type;
    private double [] rho;
    private double [] Z;
    private double [] A;
    public int nparticles;

    private double [][] shapes;
    
    private EnergyLoss [] Eloss;
    private MCS [] MultScatter;

    private double minfeaturesize;

    public Geometry(double featuresize)
    {
        randGen = new Random();
        nshapes = 0;
        type = new int[maxShapes];
        rho = new double[maxShapes];
        Z = new double[maxShapes];
        A = new double[maxShapes];
        shapes = new double[maxShapes][];
        
        Eloss = new EnergyLoss[maxShapes];
        MultScatter = new MCS[maxShapes];

        minfeaturesize = featuresize;
    }

    public int getNshapes() { return nshapes; }

    public int AddCylinder(double R0,double phi0, double z0, 
                         double R1, double phi1, double z1,
                         double rhoin, double Zin, double Ain)
            // defines the coordinates of a cylinder in (R,phi,z)
    {
        if (nshapes >= maxShapes) {
            return -1;
        }
        
        type[nshapes] = 1;
        shapes[nshapes] = new double[6];
        shapes[nshapes][0] = R0;
        shapes[nshapes][1] = phi0;
        shapes[nshapes][2] = z0;
        shapes[nshapes][3] = R1;
        shapes[nshapes][4] = phi1;
        shapes[nshapes][5] = z1;
        

        rho[nshapes] = rhoin;
        Z[nshapes] = Zin;
        A[nshapes] = Ain;

        
        Eloss[nshapes] = new EnergyLoss(rhoin, Zin, Ain);
        MultScatter[nshapes] = new MCS(rhoin, Zin, Ain);

        nshapes++;
        return (nshapes-1);
    }
    
    public void Print()
    {
        System.out.println("Geometry set up: \nStored " + getNshapes() + " objects.");
        for (int i = 0; i < nshapes; i++) {
            if (i == 0) {
                System.out.println("Maximum size of experiment given by object 0:");
            }
            if (type[i] == 1) {
                System.out.println("Geometry object #" + i + " = cylinder.");
                System.out.printf("   cylindrical coordinates (%f, %f, %f) - (%f, %f, %f)%n",
                                  shapes[i][0], shapes[i][1], shapes[i][2],
                                  shapes[i][3], shapes[i][4], shapes[i][5]);
            }
            System.out.printf("   material rho = %.3f g/cm^3, Z = %f, A = %f%n",
                              rho[i], Z[i], A[i]);
        }
        System.out.println("When scanning for volume transitions, the smallest feature size discovered will be " + minfeaturesize + "m. \n");
    }

    public boolean isInVolume(double spatialR, double spatialphi, double z, int id)
    {
        // test if point (R,phi,z) is in volume with identifier id
        
        // abort if being asked for an undefined volume
        if (id >= getNshapes()) {
            return false;
        }
        double phi = Math.PI * 2;
        if (type[id] == 1) {
            // cylinder
            return ( shapes[id][0] <= spatialR
                     && shapes[id][1] <= phi
                     && shapes[id] [2] <= z
                     && spatialR <= shapes[id][3]
                     && phi <= shapes[id][4]
                     && z <= shapes[id][5]);
        }
        
        return false;
    }
    
    public int getVolume(double x, double y, double z)  // volume uses spatial phi.
    {
        // cycle through volumes in opposite order
        double spatialR = Math.sqrt(Math.pow(x, 2)+Math.pow(y,2));
        double spatialphi = Math.atan2(x,y);
        for (int i = getNshapes()-1; i >= 0; i--) {
            if (isInVolume(spatialR, spatialphi, z, i)) {
                return i;
            }
        }
        
        // if we arrived here, we are outside everything
        return -1;
    }

    public boolean isInVolume(Particle p, int id)
    {
        // test if particle p is currently in volume with identifier id
        return isInVolume(p.spatialR(), p.spatialphi(), p.z, id);
    }
    
    public int getVolume(Particle p)
    {
        // get the highest volume number the particle is in
        return getVolume(p.x, p.y, p.z);
    }
    
    public void doEloss(Particle p, double dist)
    {
        int volume = getVolume(p);
        
        if (volume >= 0) {
            double lostE = Eloss[volume].getEnergyLoss(p)*dist;
            p.reduceEnergy(lostE);
        }
    }
    
    public void doMultScatter(Particle p, double dist)
    {
        int volume = getVolume(p);
        
        if (volume < 0) {
            return;
        }
        double scale = 1;
        double theta0 = MultScatter[volume].getTheta0(p, dist)*scale;

        if (theta0 != 0.) {
            p.applySmallRotation(randGen.nextGaussian()*theta0,
                                 randGen.nextGaussian()*theta0);
        }
        
    }

    public double [][] detectParticles(Track simtrack)
    {
        double [][] detection = new double[getNshapes()][4];
        
        // loop over each volume and average over the matching points
        for (int idet = 0; idet < getNshapes(); idet++) {

            // count points in volume
            int ncross = 0;

            for (int ipoint = 0; ipoint < simtrack.lastpos; ipoint++) {
                if (getVolume(simtrack.x[ipoint], simtrack.y[ipoint], simtrack.z[ipoint]) == idet) {
                        detection[idet][0] += simtrack.t[ipoint];
                        detection[idet][1] += simtrack.x[ipoint];
                        detection[idet][2] += simtrack.y[ipoint];
                        detection[idet][3] += simtrack.z[ipoint];
                        ncross++;
                     
                    }
                }
                      
            if (ncross > 0) {
                for (int i = 0; i < 4; i++) {                    
                        detection[idet][i] /= ncross; 
                        
                    }
                }
            }
        
        return detection;     
    }

    public double scanVolumeChange(Particle p, Particle pold, int lastVolume)
    {
        // scan the straight line between last and new position, if we missed a
        // feature of the experiment
        double dist = p.distance(pold);
        int nsteps = (int) (dist/minfeaturesize) + 1;

        if (nsteps <= 1) {
            // step was small enough, continue
            return 1.;
        }
        
        double [] pos = {pold.x, pold.y, pold.z};
        double [] end = {p.x, p.y, p.z};
        double [] delta = new double[3];
        for (int i = 0; i < 3; i++) {
            delta[i] = (end[i]-pos[i])/nsteps;
        }
        for (int n = 1; n <= nsteps; n++) {
            double x = pos[0]+n*delta[0];
            double y = pos[1]+n*delta[1];
            double z = pos[2]+n*delta[2];
            
            if (getVolume(x, y, z) != lastVolume) { 
                if (n == 1) {
                    return (0.5 / nsteps);
                } else {
                    return (n-1.0) / nsteps;
                }
            }
        }
        return 1.;
    }
}
