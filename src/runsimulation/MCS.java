package runsimulation;


class MCS
{
    double rho;
    double Z;
    double A;
    
    public MCS(){
        rho = A = Z = 0; //if no input
    }
    
    public MCS(double rho_in, double Z_in, double A_in)
    {
        rho = rho_in;
        Z= Z_in;
        A = A_in;
    }

    public double getX0()
    {
        // shall return X0 in m
        if (rho  == 0 && A==0 && Z == 0){ //if no input in Z, rho and A
            return 0;
        }else{
        double X0 = 0.01*(716.4*A)/(rho*Z*(Z+1)*Math.log(287/Math.sqrt(Z))); //radiation length
        return X0;
        }
    }

    public double getTheta0(Particle part, double x)
    {
        // shall return Theta0 for material thickness x
        if (rho  == 0 && A==0 && Z == 0){ //if no input in Z, rho and A
            return 0;
        }else{
        double Mth= x/getX0(); //material thickness in radiation lengths
        double theta0 = (13.6/(part.beta()*part.momentum()))*part.Q*Math.sqrt(Mth)*(1+0.038*Math.log(Mth));
        return theta0;
        }
    }
}