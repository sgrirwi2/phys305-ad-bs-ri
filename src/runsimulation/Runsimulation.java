package runsimulation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.pow; 
import static java.lang.Math.sin;
import java.util.Random;

class RunSimulation
{   
    // Program to run a simulation of particles in a "experiment"
    // * RunSimulation uses Particle objects to store the physical parameters of the particles generated, simulated and detected. 
    // * It creates a Geometry object to set up the experiment and uses ParticleTracker to simulate the propagation of the particles in the geometry created.
    // * The tracker includes the effect of the B-field and calls on Geometry to implement the effect of multiple scattering through the class MCS 
    //   and of energy loss due to ionisation through the class EnergyLoss. 
    // * The tracker then stores the momenta and positions of the particles on Track objects, which can then be analysed to obtain the invariant mass 
    //   from a set of four particles.
    // * The Histogram class stores data in csv files to produce histograms with external software. 
    
    // Geometry:
    // * An experiment is a collection of "volumes", that are numbered with a unique
    //   identifier from 0 to the number of experimental features.
    // * All experiemntal features are cylinders of different sizes and materials,
    //   with the sides aligned to the coordinate system axes. 
    // * Internally to the "Geometry" class are two helper classes that implement the
    //   formulas for calculation of energy loss (class "Energy loss")
    //   and multiple scattering angles (class "MCS")
    // * The main functionality is to check, if a certain particle is inside a certain volume
    //   and apply energy loss and multiple scattering during simulation
    // * The class also provides a simple mechanism to detect changes in the volume as
    //   the particle propagates and suggest an adapted step length to keep one step within
    //   one volume (the granularity of this scan is adjusted with "minfeaturesize")
    // * At the end, the class is used to "detect" particle signals in three detectors
    
    // At the end of the simulation of each event, one may analyse:
    //    * Generated particles (Particles_gen)
    //    * Simulated particles (Particles_sim) - these include the effect of energy loss and
    //      multiple scattering
    //    * Detector response (Particles_det) - these provide measurement points that can be
    //      further used to reconstruct particles like in a real experiment, where
    //      Particles_gen and Particles_sim are initially unknown

    // parameters used for ParticleTracker 
    static final double time = 1E-6; // total time to track (seconds)
    static final int nsteps = 10000; // number of time steps
    static final boolean useRungeKutta4 = true; // use/don't use RK4
    private static Random randGen = new Random(); // for resolution of the detectors

    // minimum size of experimental features,
    static final double minfeaturesize = 0.001; // ~10 smaller than the thinest elements of the experiment

    // start values
    static final double startMomentum = 1000.; // MeV 
    static final double startAngle = 0.; // Radians 
    
    // number of events to simulate
    static final int numberOfEvents = 1; // 2022 tot;
    
    
    public static void main (String [] args ) {

        //checking directory
        String currentDirectory = System.getProperty("user.dir");
        System.out.println("The current working directory is " + currentDirectory);
        
        // setup histograms for analysis
        // momentum
        Histogram hist_gen_mom = new Histogram(50, 0., startMomentum*1.01, "initial generated Momentum");
        Histogram hist_sim_mom = new Histogram(50, 0., startMomentum*1.01, "simulated final Momentum");
        // theta
        Histogram hist_gen_theta_zx = new Histogram(50, startAngle-2*Math.PI, startAngle+2*Math.PI, "initial generated Theta z-x");
        Histogram hist_gen_theta_zy = new Histogram(50, -2*Math.PI, 2*Math.PI, "initial generated Theta z-y");
        Histogram hist_sim_theta_zx = new Histogram(50, startAngle-2*Math.PI, startAngle+2*Math.PI, "simulated Theta z-x");
        Histogram hist_sim_theta_zy = new Histogram(50, -2*Math.PI, 2*Math.PI, "simulated Theta z-y");
        Histogram hist_det_theta_zx = new Histogram(50, startAngle-2*Math.PI, startAngle+2*Math.PI, "measured theta z-x");
        Histogram hist_det_theta_zy = new Histogram(50, -2*Math.PI, 2*Math.PI, "measured theta z-y");
        // invariant mass
        Histogram hist_inv_M_gen = new Histogram(100, 60000, 600000, "invariant mass of generated particles");
        Histogram hist_inv_M_sim = new Histogram(100, 60000, 600000, "invariant mass of simulated particles");
        Histogram hist_inv_M_det = new Histogram(100, 60000, 600000, "invariant mass of detected particles");
        
        // experiment set up
        Geometry Experiment = SetupExperiment(); // Define geometry of experiment in method SetupExperiment()
        Experiment.nparticles = 4; // set the number of particles per event
        int success = 0; // counter for successful detections
        int success2 = 0; // counter for detection of 4 particles 
        
        // start of main loop: run the simulation numberOfEvents times
        for (int nev = 0; nev < numberOfEvents; nev++) {

            if (nev % 1000 == 0) {
                System.out.println("Simulating event " + nev + "\n");
            }

            //read csv file to store particles read from line 'nev'
            Particle [] Particles_gen = readCSV(nev, Experiment.nparticles); // inputs number of events and number of particles
            
            //invariant mass of generated particles
            System.out.println("Generated particles' invariant mass");
            double invariant_M_gen = invariant_M(Particles_gen);
            hist_inv_M_gen.fill(invariant_M_gen);
            
            // simulate propagation of each generated particle,
            // store output in Particles_sim and Tracks_sim
            Particle [] Particles_sim = new Particle[Experiment.nparticles];
            Track [] Tracks_sim = new Track[Experiment.nparticles];

            for (int ip = 0; ip < Experiment.nparticles; ip++) {
                // some output (need to disable before running large numbers of events!)
                System.out.println("Simulating particle " + ip + " of event " + nev);
                Particles_gen[ip].print();

                ParticleTracker tracker = new ParticleTracker(Particles_gen[ip], time, nsteps, useRungeKutta4);

                System.out.println("Output particle: ");
                Particles_sim[ip] = tracker.track(Experiment); // = particle ip
            

                // save the full simulated track for later analysis
                Tracks_sim[ip] = tracker.getTrack();  // = track of particle ip
                

                // write scatter plot for event 0, particle 0 to disk into file "output_particle.csv"
                if (nev == 0 && ip == 0) {
                    Tracks_sim[ip].writeToDisk("output_particle.csv");
                }
            }
            // end of simulated particle propagation
            
            //invariant mass of simulated particles
            //comment out below when working
            System.out.println("Simulated particles' invariant mass");
            double invariant_M_sim = invariant_M(Particles_sim);
            hist_inv_M_sim.fill(invariant_M_sim);
            
            // simulate detection of each particle in each element from the simulated tracks
            // at this stage the simulation is done and we analyse the output
            Particle [] Particles_det = new Particle[Experiment.nparticles]; // array to store reconstructed particles' values
            int counter = 0; // counter to ensure only sets of 4 successfully detected particles are used
            for (int ip = 0; ip < Experiment.nparticles; ip++) {
                
                // retrieve initial generated particle momentum and fill histogram
                hist_gen_mom.fill(Particles_gen[ip].momentum());
                // retrieve simulated particle momentum at the end of the simulation and fill histogram
                hist_sim_mom.fill(Particles_sim[ip].momentum());
                
                // calculate theta angles in the z-x and z-y planes
                // generated - this should be equal to given startAngle and zero
                double gen_theta_zx = Math.atan2(Particles_gen[ip].px, Particles_gen[ip].pz);
                hist_gen_theta_zx.fill(gen_theta_zx);
                double gen_theta_zy = Math.atan2(Particles_gen[ip].py, Particles_gen[ip].pz);
                hist_gen_theta_zy.fill(gen_theta_zy);
                
                // same after simulation - muon will have scattered around a bit
                double sim_theta_zx = Math.atan2(Particles_sim[ip].px, Particles_sim[ip].pz);
                hist_sim_theta_zx.fill(sim_theta_zx);
                double sim_theta_zy = Math.atan2(Particles_sim[ip].py, Particles_sim[ip].pz);
                hist_sim_theta_zy.fill(sim_theta_zy);
                
                // after detection: reconstruct the angle from the three detected positions!
                double [][] detection_txyz = Experiment.detectParticles(Tracks_sim[ip]); // get coordinates of particle ip in detectors
                // apply set resolution
                double resol_det = 0; // resolution - set to 0 for no effect
                // retrieve straight line reconstruction, detector 2 coord for reference and number of hits
                double [] mcd2 = straightLine(detection_txyz, ip, resol_det); 
                
                // sum up total successful detections for efficiency calculations
                success = success + (int) Math.round(mcd2[5]); 
                
                // only runs if line equation exists (if particle ip was detected)
                if (mcd2[0]!=0) { // if gradient is not 0
                    
                    double [] intersect = intersectLine(mcd2, 1.2); // Radius 1.2 m 
                    
                    //determine parameters:
                    System.out.println("\nParameters calculated for particle number = " + ip);
                    //find dotted line
                    double grad;
                    grad = intersect[1]/intersect[0];
                    System.out.println("The equation of the dotted line is y = "+grad+ "x");

                    //find angle between 2 lines
                    double delta;
                    delta = Math.atan((mcd2[0]-grad)/(1+mcd2[0]*grad));
                    System.out.println("The value of delta is " + delta);

                    // determine normal momentum
                    double charge = delta / Math.abs(delta);
                    double norm_mom =( 300 * 4 * 1.2 * charge )/ (2* delta);
                    System.out.println("The normal momentum value is " + norm_mom );
                    
                    //using correct intersection points, phi angle in x-y plane
                    double phixy; // reconstructed phi
                    phixy = atan2(intersect[1], intersect[0]);
        
                    //using coord points of detector shape 2, theta in rz plane
                    double mcd2r = Math.sqrt(Math.pow(mcd2[2], 2)+Math.pow(mcd2[3], 2)); //rcoord
                    
                    double thetarz; //theta in the rz plane
                    thetarz = atan2(mcd2r, mcd2[4]); // atan2(rcoord,zcoord)
                    
                    //p used to find pz later
                    double p;
                    p = norm_mom / sin(thetarz);
                    
                    //the reconstructed px, py, pz , we have variables called px, py, pz earlier so trying to avoid confusion
                    double pxt;
                    double pyt;
                    double pzt;
                    
                    
                    pxt = cos(phixy+delta) * norm_mom;
                    pyt = sin(phixy+delta) * norm_mom;
                    pzt = cos(thetarz) * p;
                    
                    System.out.println("Generated momentum: px = " + Particles_gen[ip].px + ", py = " + Particles_gen[ip].py + ", pz = " + Particles_gen[ip].pz );
                    System.out.println("Reconstructed momentum: px = " + pxt + ", py = " + pyt + ", pz = " + pzt + "\n");
                    //end of edits, only printed above line for clarity, feel free to edit
                    
                    // store particle values in array at position ip (for invariant mass method)
                    Particles_det[ip] = new Particle(); // creates particle with no values at position i
                    Particles_det[ip].px = pxt;
                    Particles_det[ip].py = pyt;
                    Particles_det[ip].pz = pzt;
                    Particles_det[ip].m = 106; // [MeV] = mass of a muon
                    counter++; // successful calculation adds up
                    
                } else { // if the gradient was set to 0 (not enough particle detections)
                    System.out.println("\nThe coordinates of particle " + ip + " were not sufficient to determine the parameters in event " + nev + "\n");
                }

                // end of analysis
            }
            
            // invariant mass of detected particles
            if (counter==4) { // only if 4 detections were successful
                System.out.println("Detected particles' invariant mass");
                double invariant_M_det = invariant_M(Particles_det);
                hist_inv_M_det.fill(invariant_M_det);
                success2++; // count successful reconstruction
            } else {
                System.out.println("The detections of event " + nev + " were not sufficient to reconstruct the invariant mass\n");
            }
        
        }
        // end of main event loop
        // efficiency of simulation
        double tot_det = numberOfEvents*3.*4.; // total possible hits
        double eff_det = success/tot_det; // efficiency of individual detections
        double eff_rec = success2/(double)numberOfEvents; // efficiency of reconstruction
        System.out.println("End of analysis. Summary below. \nTotal number of hits: " + success + " of " + (int)tot_det + " expected \nEfficiency of detection: " + eff_det + "\nTotal number of successful invariant mass calculations: " + success2 + " of " + numberOfEvents + " events \nEfficiency of reconstruction: " + eff_rec);
        // write out histograms for plotting and futher analysis
        // momentum
        hist_gen_mom.writeToDisk("gen_mom.csv");
        hist_sim_mom.writeToDisk("sim_mom.csv");
        // theta
        hist_gen_theta_zx.writeToDisk("gen_theta_zx.csv");
        hist_gen_theta_zy.writeToDisk("gen_theta_zy.csv");
        hist_sim_theta_zx.writeToDisk("sim_theta_zx.csv");
        hist_sim_theta_zy.writeToDisk("sim_theta_zy.csv");
        hist_det_theta_zx.writeToDisk("det_theta_zx.csv");
        hist_det_theta_zy.writeToDisk("det_theta_zy.csv");
        // invariant mass
        hist_inv_M_gen.writeToDisk("inv_M_gen.csv");
        hist_inv_M_sim.writeToDisk("inv_M_sim.csv");
        hist_inv_M_det.writeToDisk("inv_M_det.csv");

    }
    
    public static Geometry SetupExperiment () {
        
        final double ironThickness = 1.; //m
        final double phi = 2*Math.PI;
        Geometry Experiment = new Geometry(minfeaturesize);
        double d_length = 10;
        // this line defines the size of the experiment in vacuum
        Experiment.AddCylinder(0., -phi, -d_length , // start R, phi, z
                               2.50025, phi , d_length, // end   R, phi, z
                               0.,0., 0.); // zeros for "vacuum"

        // block of iron: 1x5 m^2 wide in R,z-direction, ironThickness m thick in R-direction
        Experiment.AddCylinder(1.2,-phi,-d_length, // start R, phi, z
                             1.2+ironThickness,phi, d_length, // end   R, phi, z
                             7.87, 26, 55.845); // = iron values [density, Z, A]//7.87 26 55.845
        
        // three 1mm-thin "silicon detectors" 10cm, 20cm and 30cm after the iron block
        Experiment.AddCylinder( 2.3, -phi, -d_length, // start R, phi, z
                             2.31, phi, d_length, // end  R, phi, z
                             2.33, 14 ,28.085); // density, Z, A//2.33 14 28.085
        
        Experiment.AddCylinder(2.4, -phi, -d_length, // start R, phi, z
                             2.41, phi, d_length,   // end   R, phi, z
                             2.33, 14 ,28.085); // density, Z, A
        
        Experiment.AddCylinder(2.5, -phi, -d_length, // start R, phi, z
                             2.51, phi, d_length, // end   R, phi, z
                             2.33, 14 ,28.085); // density, Z, A
        
        Experiment.Print();

        return Experiment;
        
    }

    
    public static Particle [] readCSV(int lineNumber, int nparticles) {
        
        String csvFile =  "C:\\Users\\aster\\Google Drive\\Comp_model\\HighMassH4l.csv"; // "C:\\Users\\Beth\\Documents\\NetBeansProjects\\projectfinal2\\phys305-ad-bs-ri\\HighMassH4l.csv"; // "C:\\Users\\Rebecca\\Documents\\NetBeansProjects\\HighMassH4l.csv"; // directory of csv file
        String line = "";
        String cvsSplitBy = ",";
        
        Particle [] Particles_gen = new Particle[nparticles];
        
        try (BufferedReader reader = new BufferedReader(new FileReader(csvFile))) {
        int j =-1;
            while ((line = reader.readLine()) != null) {
                    j++;
                    if (j == lineNumber) {
                        //  System.out.println(line); //Print line to check                
                        // use comma as separator
                        String[] ParticleVal = line.split(cvsSplitBy);  
                        //Store properties in separate particle objects in an array
                        for (int i = 0; i<=3; i++){
                            Particles_gen[i] = new Particle(); // creates particle with no values at position i
                            Particles_gen[i].px = Integer.parseInt(ParticleVal[4*i]);
                            Particles_gen[i].py = Integer.parseInt(ParticleVal[4*i+1]);
                            Particles_gen[i].pz = Integer.parseInt(ParticleVal[4*i+2]);
                            Particles_gen[i].Q = Integer.parseInt(ParticleVal[4*i+3]);
                            Particles_gen[i].m = 106;
                            Particles_gen[i].spatialR = Particles_gen[i].spatialR();
                            Particles_gen[i].spatialphi = Particles_gen[i].spatialphi();
                            Particles_gen[i].inphi = Particles_gen[i].inphi();
                            Particles_gen[i].normalmomentum = Particles_gen[i].normalmomentum();
                            // System.out.println("Particle " + i + ", input from line " + lineNumber);
                            Particles_gen[i].print();
                        }
                        break;
                    } else { // only used to check code
//                        System.out.println("Skip line " + j);
                    }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return Particles_gen;
        
    }
    
    public static double invariant_M(Particle[] p) {
        double sumE = 0;
        double sumPx = 0;
        double sumPy = 0;
        double sumPz = 0;
        // sums up all E, px, py, pz of the particles
        for (int i = 0; i < p.length; i++){
            sumE = sumE + p[i].E();
            sumPx = sumPx + p[i].px;
            sumPy = sumPy + p[i].py;
            sumPz = sumPz + p[i].pz;
        }
        // calculates inv mass
        double M_sq = Math.pow(sumE,2)-Math.pow(sumPx,2)-Math.pow(sumPy,2)-Math.pow(sumPz,2);
        double inv_M = Math.sqrt(M_sq);
        System.out.println("Invariant mass of " + p.length + " particles calculated = " + inv_M + " MeV \n");
        return inv_M;
    }

    public static double [] straightLine (double [][] coordinates, int particle, double resol) {
        // define variables for fit
        double xcoord = 0;
        double ycoord = 0;
        double zcoord = 0;
        double sum_x = 0;
        double sum_y = 0;
        double sum_xy = 0;
        double sum_x2 = 0;
        double m,c;
        
        System.out.println("Fitting of particle number = " + particle);
        // make counter to check for successful detections
        int counter = 0;
        // store coordinates in separate arrays for x, y, z for each detector
        for (int a = 2; a < 5; a++  ) {
                    
            xcoord = coordinates[a][1]; // xcoord 
            ycoord = coordinates[a][2]; // ycoord
            zcoord = coordinates[a][3]; // zcoord
            
            System.out.println("Coordinates in detector " + a + " are: x = " + xcoord + " y = " + ycoord + " z = " + zcoord); 
            
            // check that both x and y are not 0 (particle was detected)
            if (xcoord!=0 && ycoord!=0) {
                
                // add resolution defined by resol
                xcoord = xcoord + randGen.nextGaussian()*resol; 
                ycoord = ycoord + randGen.nextGaussian()*resol; 
                
                // sum up over each detector's coordinates for the straight line fit
                sum_x = xcoord + sum_x ;
                sum_y = sum_y + ycoord ;
                sum_xy = sum_xy + (xcoord * ycoord);
                sum_x2 = sum_x2 + Math.pow(xcoord,2);
                
                counter++; //should add up to 3 if detected by 3 detectors
            }
            
        }
        // end of loop, sum for particle ip allows to fit a straight line
        
        // debug lines
        //System.out.println(sum_x);
        //System.out.println(sum_y);
        //System.out.println(sum_xy);
        //System.out.println(sum_x2);
                        
        // fitted straight line
        if (counter > 1) { // only runs if particles were detected by at least 2 detectors
            m = (counter * sum_xy - sum_x * sum_y) / (counter * sum_x2 - pow(sum_x, 2)); 
            c = (sum_y - (m * sum_x)) / counter;
            System.out.println("The equation of the fitted line is y = " + m +" x + " + c);
        } else {
            System.out.println("The particle was not detected by any detector");
            m = c = 0; // sets gradient and intercept to 0
        }
        
        // create output array with m, c, coordinates of detector 2 and number of detections
        double [] m_c_d2 = new double [6];
        m_c_d2[0] = m;
        m_c_d2[1] = c;
        m_c_d2[2] = coordinates[2][1]; // x coordinates of shape 2
        m_c_d2[3] = coordinates[2][2]; // y coordinates of shape 2
        m_c_d2[4] = coordinates[2][3]; // z coordinates of shape 2
        m_c_d2[5] = counter; // number of successful detections
        
        return m_c_d2;
        
    }
    
    public static double [] intersectLine(double [] line_eq, double Rint) {
        // define variables
        double xint, yint, xint2, yint2 = 0; //for interesect points
        double m = line_eq[0];
        double c = line_eq[1];
        double d2x = line_eq[2];
        double d2y = line_eq[3];
        double d2z = line_eq[4]; 
        
        //two equations for postive (y) circle region and negative
        xint = ((-Math.sqrt((-Math.pow(c,2))+(Math.pow(m,2)*Math.pow(Rint,2))+(Math.pow(Rint,2))))-(c*m))/((m*m)+1);
        yint = (m*xint)+c;
//        System.out.println("Equation 1 intersect point with Radius at x = " + xint + ", y = " + yint); // debug check

        xint2 = ((Math.sqrt((-Math.pow(c,2))+(Math.pow(m,2)*Math.pow(Rint,2))+(Math.pow(Rint,2))))-(c*m))/((m*m)+1);
        yint2 = (m*xint2)+c;
//        System.out.println("Equation 2 intersect point with Radius at x = " + xint2 + ", y = " + yint2 + "\n"); // debug check
        
        // compare coordinates of intersect with detector 2
        double xy1_d2 = Math.sqrt(Math.pow((yint-d2y),2)+Math.pow((xint-d2x),2)); // magnitude of vector drawn from detector to intersect 1
        double xy2_d2 = Math.sqrt(Math.pow((yint2-d2y),2)+Math.pow((xint2-d2x),2)); // magnitude of vector drawn from detector to intersect 2
        // choose smallest magnitude set 
        if (xy2_d2<xy1_d2) {
            xint = xint2;
            yint = yint2;
        }
        
        // prints out correct equation only
        System.out.println("Intersect point with Radius at x = " + xint + ", y = " + yint);
        
        // create array with intersect coordinates
        double [] xy_inter = new double [2];
        xy_inter[0] = xint;
        xy_inter[1] = yint;
        
        return xy_inter;

    }
    
}
            
