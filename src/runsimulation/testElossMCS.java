/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package runsimulation;

class testElossMCS
{
    // minimal test program to test EnergyLoss and MCS classes
    // uses Particle class to set up some simple test cases

    public static void main (String [] args )
    {
	// set up EnergyLoss class with iron parameters
	EnergyLoss ironEloss = new EnergyLoss(19.3,74,183.84);
        //(7.87, 26, 55.845);
	
	// set up a little loop to run a test with a few different momenta
	double [] momenta = {30, 300, 3000, 10000, 30000, 100000}; // MeV
	for (int i = 0; i < momenta.length; i++) {
	    // setup test particle
	    Particle particle = new Particle();
	    particle.Q = +1;
	    particle.m = 106.; // MeV
	    particle.px = momenta[i];

	    // get dE/dx for this particle in iron and print
	    double dEdx_Iron = ironEloss.getEnergyLoss(particle);
	    System.out.printf("dE/dx = %.0f MeV/m for p = %.1f MeV%n", 
			      dEdx_Iron, particle.momentum());
	}

	// multiple scattering for 1cm
	double thickness = 0.01; //m

	// set up MCS class with iron parameters
	MCS ironMCS = new MCS(19.3,74,183.84);
        //(7.87, 26, 55.845);

	System.out.printf("Iron X0 %.5f m%n", ironMCS.getX0());

	// set up a little loop to run a test with a few different momenta
	for (int i = 0; i < momenta.length; i++) {
	    // setup test particle
	    Particle particle = new Particle();
	    particle.Q = +1;
	    particle.m = 106.; // MeV
	    particle.px = momenta[i];
	    particle.x = 0.;

	    // interface takes instance of muon and distance of last step
	    double theta0 = ironMCS.getTheta0(particle, thickness);

	    System.out.printf("theta0 = %.5f for p = %.1f MeV and %.3f m thickness%n",
			      theta0, particle.momentum(), thickness);
	}
	

    }
}